import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import formatTime from './common/formatTime';
import formatIntervalTime from './common/formatIntervalTime';


function CreateWorkout() {
    const [time, setTime] = useState(0)
    const [intervalTime, setIntervalTime] = useState(0)
    const [lastClickTime, setLastClickTime] = useState(Date.now())
    const [splitTimes, setSplitTimes] = useState([])
    // const [abandonInitiated, setAbandonInitiated] = useState(false)
    const [counter, setCounter] = useState({
        pull_ups: 0,
        push_ups: 0,
        squats: 0,
    })

    // const { token } = useAuthContext()
    const workoutKeys = [
        'Run 1',
        'Set 1',
        'Set 2',
        'Set 3',
        'Set 4',
        'Set 5',
        'Set 6',
        'Set 7',
        'Set 8',
        'Set 9',
        'Set 10',
        'Run 2',
    ]
    const workoutTotals = {
        pull_ups: 100,
        push_ups: 200,
        squats: 300,
    }
    const setTotals = {
        pull_ups: 10,
        push_ups: 20,
        squats: 30,
    }
    const modalConfig = {
        title: `Abandon workout`,
        bodyText: `Are you sure? We believe in you! Summon your inner 12th man!\n\nIf you stop your workout early, you will be redirected to your dashboard.`,
        confirmButtonText: `Abandon Workout`,
        cancelButtonText: `Go Back`,
    }

    useEffect(() => {
        setIntervalTime(0)
        const intervalTimer = setInterval(() => {
            setIntervalTime((prevTime) => prevTime + 1)
        }, 1000)
        const sumSplitTimes = splitTimes.reduce((a, b) => a + b, 0)
        setTime(sumSplitTimes)
        return () => {
            clearInterval(intervalTimer)
        }
    }, [splitTimes])

    const handleFinishSet = () => {
        const now = Date.now()
        const timeSinceLastClick = now - lastClickTime

        // If less than 2 seconds has passed since the last click, ignore this click
        if (timeSinceLastClick < 2000) {
            return
        }
        // otherwise, reset the lastClickTime, set the splitTimes, and increment the counter
        setLastClickTime(now)
        setSplitTimes((prevSplitTimes) => [...prevSplitTimes, intervalTime])
        if (splitTimes.length > 0 && splitTimes.length < 11) {
            setCounter({
                pull_ups: counter.pull_ups + setTotals.pull_ups,
                push_ups: counter.push_ups + setTotals.push_ups,
                squats: counter.squats + setTotals.squats,
            })
        }
    }

    // const handleFinishWorkout = async () => {
    //     const now = Date.now()
    //     const timeSinceLastClick = now - lastClickTime
    //     // If less than 2 seconds has passed since the last click, ignore this click
    //     if (timeSinceLastClick < 2000) {
    //         return
    //     }
    //     // Otherwise, set the split time, create a workout object, and submit the post request
    //     setSplitTimes((prevSplitTimes) => [...prevSplitTimes, intervalTime])

    //     const workout = {
    //         date: getTodaysDate(),
    //         run_1: splitTimes[0],
    //         set_1: splitTimes[1],
    //         set_2: splitTimes[2],
    //         set_3: splitTimes[3],
    //         set_4: splitTimes[4],
    //         set_5: splitTimes[5],
    //         set_6: splitTimes[6],
    //         set_7: splitTimes[7],
    //         set_8: splitTimes[8],
    //         set_9: splitTimes[9],
    //         set_10: splitTimes[10],
    //         run_2: intervalTime,
    //     }
    // //     const workoutURL = `${API_HOST}/api/workouts`
    // //     const fetchConfigPost = {
    // //         method: 'post',
    // //         body: JSON.stringify(workout),
    // //         headers: {
    // //             'Content-Type': 'application/json',
    // //         },
    // //         credentials: 'include',
    // //     }

    // //     const response = await fetch(workoutURL, fetchConfigPost)
    // //     if (response.ok) {
    // //         navigate('/dashboard')
    // //     }
    // // }

    // const handleModalDisplay = () => {
    //     setAbandonInitiated(!abandonInitiated)
    // }

    // const handleConfirmAbandonWorkout = async () => {
    //     const workout = {
    //         date: getTodaysDate(),
    //         run_1: splitTimes[0],
    //         set_1: splitTimes[1],
    //         set_2: splitTimes[2],
    //         set_3: splitTimes[3],
    //         set_4: splitTimes[4],
    //         set_5: splitTimes[5],
    //         set_6: splitTimes[6],
    //         set_7: splitTimes[7],
    //         set_8: splitTimes[8],
    //         set_9: splitTimes[9],
    //         set_10: splitTimes[10],
    //         run_2: null,
    //     }
    //     // const workoutURL = `${API_HOST}/api/workouts`
    //     // const fetchConfigPost = {
    //     //     method: 'post',
    //     //     body: JSON.stringify(workout),
    //     //     headers: {
    //     //         'Content-Type': 'application/json',
    //     //     },
    //     //     credentials: 'include',
    //     // }

    //     // const response = await fetch(workoutURL, fetchConfigPost)
    //     // if (response.ok) {
    //     //     navigate('/dashboard')
    //     // }
    // // }

    // // if (!token) {
    // //     return <Navigate to="/login" />
    // }
  return (
    <>
      {/* {abandonInitiated ? (
        <Modal
          textConfig={modalConfig}
          confirmFn={handleConfirmAbandonWorkout}
          cancelFn={handleModalDisplay}
        />
      ) : (
        ''
      )} */}

      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>Let's do a Murph!</Text>
        </View>
        <View style={styles.splitContainer}>
          {/* Workout Splits */}
          <View style={styles.workoutSplitsContainer}>
            <Text style={styles.workoutSplitsText}>Workout Splits</Text>
            <View style={styles.tableContainer}>
              <Text style={[styles.tableHeader, styles.tableCell]}>Split</Text>
              <Text style={[styles.tableHeader, styles.tableCell]}>Time</Text>
              {workoutKeys.map((value, index) => (
                <View
                  key={index}
                  style={[
                    styles.tableRow,
                    index % 2 === 0 ? styles.tableRowEven : styles.tableRowOdd,
                  ]}
                >
                  <Text style={[styles.tableCell, styles.splitLabel]}>{value}</Text>
                  <Text style={styles.tableCell}>
                    {splitTimes[index]
                      ? formatIntervalTime(splitTimes[index])
                      : index === 0 || splitTimes[index - 1]
                      ? formatIntervalTime(intervalTime)
                      : '--'}
                  </Text>
                </View>
              ))}
            </View>
          </View>

          {/* Workout Totals/Remaining */}
          <View style={styles.workoutTotalsContainer}>
            <Text style={styles.workoutTotalsText}>Workout Totals/Remaining</Text>
            <View style={styles.statsContainer}>
              <View style={styles.statsColumn}>
                <Text>Reps per set:</Text>
                <Text>Pull-ups: 10</Text>
                <Text>Push-ups: 20</Text>
                <Text>Squats: 30</Text>
              </View>
              <View style={styles.statsColumn}>
                <View style={styles.textBox}>
                  <Text style={styles.textBoxLabel}>Timer</Text>
                  <Text style={styles.textBoxValue}>
                    {formatTime(time + intervalTime)}
                  </Text>
                </View>
                <View style={styles.textBox}>
                  <Text>Pull-ups</Text>
                  <Text>
                    {counter.pull_ups} / {workoutTotals.pull_ups - counter.pull_ups}
                  </Text>
                </View>
                <View style={styles.textBox}>
                  <Text>Push-ups</Text>
                  <Text>
                    {counter.push_ups} / {workoutTotals.push_ups - counter.push_ups}
                  </Text>
                </View>
                <View style={styles.textBox}>
                  <Text>Squats</Text>
                  <Text>
                    {counter.squats} / {workoutTotals.squats - counter.squats}
                  </Text>
                </View>
              </View>
            </View>

            {/* Finish Workout / Finish Set buttons */}
            <View style={styles.buttonContainer}>
              {splitTimes.length >= 11 ? (
                <TouchableOpacity
                  onPress={handleFinishWorkout}
                  style={styles.finishButton}
                >
                  <Text style={styles.buttonText}>Finish Workout</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity onPress={handleFinishSet} style={styles.finishButton}>
                  <Text style={styles.buttonText}>Finish Set</Text>
                </TouchableOpacity>
              )}
              {/* <TouchableOpacity onPress={handleModalDisplay} style={styles.abandonButton}>
                <Text style={styles.buttonText}>End Workout Early</Text>
              </TouchableOpacity> */}
            </View>
          </View>
        </View>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  // Styles go here, adapting the existing styles to React Native
});

export default CreateWorkout;
