import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
// import { useNavigation } from '@react-navigation/native';

function CountdownPage({navigation}) {
  const [time, setTime] = useState(11);


  useEffect(() => {
    const redirectTimer = setTimeout(() => {
      navigation.navigate('CreateWorkout');
    }, 11000);
    return () => clearTimeout(redirectTimer);
  }, [navigation]);

  useEffect(() => {
    const timer = setInterval(() => {
      if (time > 1) setTime((prevTime) => prevTime - 1);
    }, 1000);
    return () => {
      clearInterval(timer);
    };
  }, [time]);

  return (
    <View style={styles.container}>
      <Text style={styles.heading}>Your workout begins in...</Text>
      <Text style={styles.timer}>
        {time > 1 ? time - 1 : "GO!"}
      </Text>
      {/* <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('Workout')}
      >
        <Text style={styles.buttonText}>Bypass and Begin!</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.button, { backgroundColor: 'red' }]}
        onPress={() => navigation.navigate('Dashboard')}
      > */}
        {/* <Text style={styles.buttonText}>ABORT WORKOUT</Text>
      </TouchableOpacity> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  heading: {
    marginBottom: 20,
    fontSize: 18,
  },
  timer: {
    fontSize: 80,
    color: 'green',
  },

});

export default CountdownPage;
